package dam1819.teoriauf3;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements View.OnTouchListener,
        SensorEventListener {

    //elements que necessitem per controlar els "clicks" a la pantalla
    private TextView accions, direccio;
    private TextView textValorX, textValorY, textValorZ;
    private float iniciX, iniciY;
    private float finalX, finalY;
    private float valorX, valorY, valorZ;
    private SensorManager sensorManager;
    private Sensor sensor;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, R.string.snack_bar_basic_text, Snackbar.LENGTH_LONG)
                        .setAction(R.string.snack_bar_basic_action, null).show();
            }
        });

        //agafem al linearLayout
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linerMainActivity);
        linearLayout.setOnTouchListener(this);

        //intanciem les variables
        this.accions = (TextView) findViewById(R.id.accions);
        this.direccio = (TextView) findViewById(R.id.direccio);
        this.textValorX = (TextView) findViewById(R.id.valorX);
        this.textValorY = (TextView) findViewById(R.id.valorY);
        this.textValorZ = (TextView) findViewById(R.id.valorZ);
        this.image = (ImageView) findViewById(R.id.imageView);
        this.iniciX = 0;
        this.iniciY = 0;
        this.finalX = 0;
        this.finalY = 0;
        this.valorX = 0;
        this.valorY = 0;
        this.valorZ = 0;

        //instanciem els sensors
        this.sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        this.sensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.sensorManager.registerListener(this, this.sensor , SensorManager.SENSOR_DELAY_NORMAL);

    }


    /*
        No és obligatori ni indespensable però...
        Afegirem els metodes onPause i onResume per estalviar bateria

     */

    @Override
    protected void onPause() {
        super.onPause();
        this.sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        this.sensorManager.registerListener(this,sensor, SensorManager.
                SENSOR_DELAY_NORMAL);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    opcions princiapls de MotionEvent
        ACTION_DOWN: es produeix quan l’usuari inicia una pulsació, és a dir,
            al moment exacte que el dit fa contacte en la pantalla.
        ACTION_UP: quan finalitzem la pulsació, quan el dit deixa de tocar la pantalla.
        ACTION_MOVE: entre els events ACTION_DOWN i ACTION_UP hi ha hagut algun canvi en la posició del dit,
            si hi ha moviment és perquè l’usuari ha desplaçat el dit per la pantalla durant la pulsació.
        ACTION_POINTER_DOWN: s’ha produït una pulsació d’un dit que no és el principal (passem a generar events multi touch).
            Quan aquest dit s’aixeca de la pantalla genera un ACTION_POINTER_UP.
        ACTION_POINTER_INDEX_MASK:
            conjuntament amb ACTION_POINTER_INDEX_SHIFT ens permetrà saber quin índex té la pulsació en un event multi touch.
     */

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // Registrem l’event al TextView el moment de l'inici de l'acció
            this.accions.setText(R.string.action_down);
            // Desem els moviments de les X i Y inicials
            this.iniciX = event.getX();
            this.iniciY = event.getY();
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            // Registrem l’event al TextView el moment del final de l'acció
            this.accions.setText(R.string.action_up);
            // Desem els moviments de les X i Y finals
            this.finalX = event.getX();
            this.finalY = event.getY();

            if (Math.abs(this.finalX-this.iniciX) > Math.abs(this.finalY-this.iniciY)) {
            // Si la X final és major que la inicial ha fet "drag" cap a la dreta
            if (this.finalX > this.iniciX) {
                this.direccio.setText(R.string.go_right);
                this.image.setRotation(90);
                // Si no, cap a l’esquerra
            } else {
                this.image.setRotation(270);
                this.direccio.setText(R.string.go_left);
            }
                // Si és el vertical
            }else {
                // Si la Y final és major que la inicial ha fet "drag" cap avall
                if (this.finalY > this.iniciY) {
                    this.direccio.setText(R.string.go_down);
                    this.image.setRotation(180);
                    // Si no, cap a dalt
                } else {
                    this.direccio.setText(R.string.go_up);
                    this.image.setRotation(0);
                }
        }
        }else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            // Registrem l’event al TextView
            this.accions.setText(R.string.action_move);

            //passem els valors a strings
            String stringValorX = concatenarIdValor(R.string.xPos, event.getX());
            String stringValorY = concatenarIdValor(R.string.yPos, event.getY());

            //exercici: on estic apretant?
            this.textValorX.setText(stringValorX);
            this.textValorY.setText(stringValorY);

            //seguiment del dit
            this.image.setX(event.getX()-this.image.getWidth()/2);
            this.image.setY(event.getY()-this.image.getHeight()/2);

        }
        return true;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            //agafem els valors del sensor
            this.valorX = event.values[0];
            this.valorY = event.values[1];
            this.valorZ = event.values[2];

            //passem els valors a strings
            String stringValorX = concatenarIdValor(R.string.xPos, this.valorX);
            String stringValorY = concatenarIdValor(R.string.yPos, this.valorY);
            String stringValorZ = concatenarIdValor(R.string.zPos, this.valorZ);

            //posem els valors als textViews
            this.textValorX.setText(stringValorX);
            this.textValorY.setText(stringValorY);
            this.textValorZ.setText(stringValorZ);

            //exercici: direcció cap on moc el dispositiu?
                //si X > Y
            if(this.valorX > this.valorY){
                if(this.valorY > this.valorZ){ // si X > Y > Z
                    //
                }else{ // si X > Z > Y
                    //
                }
                }
            }else if(this.valorY > this.valorZ){
                if(this.valorX > this.valorZ) { // si Y > X > Z
                    //
                }else{ // si Y > Z > X
                    //
                    }
            }else{
                if(this.valorZ > this.valorY){
                    if(this.valorX > this.valorY){ // si Z > X > Y
                        //
                    }else{// si Z > Y > X
                        //
                    }
                }
            }
        }

    private String concatenarIdValor(int id, float valor){
        return getText(id)+ " " + String.valueOf(valor);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
